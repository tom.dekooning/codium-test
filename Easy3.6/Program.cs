﻿using System;
using System.Text;

namespace Easy3._6
{
    class Program
    {
        private static int[] numbers = {1, 2, 3, 4, 5, 10, 15};

        static void Main(string[] args)
        {
            foreach (var t in numbers)
            {
                ProduceOutput(t);
            }
        }

        private static void ProduceOutput(int n)
        {
            Console.WriteLine($"n={n}");
            if (n == 1) Console.Write("+");

            StringBuilder top = new StringBuilder();
            StringBuilder middle = new StringBuilder();
            StringBuilder bottom = new StringBuilder();

            if (n > 1)
            {
                // Generation middle
                int amountEMiddle = 1;
                if (n == 2) amountEMiddle = 1;
                else  amountEMiddle += (n - 2)*2;

                middle = middle.Append("+" + new string('E', amountEMiddle) + "+");

                // Generation top
                int amountA = n - 1;
                int amountB = n - 1;
                int amountETop = 0;
                while (amountETop < amountEMiddle)
                {
                    top = top.Append(new string('A', amountA) + "+");
                    if (amountETop > 0) top = top.Append(new string('E', amountETop) + "+");
                    top = top.Append(new string('B', amountB) + "\n");

                    if (amountETop == 0) amountETop++;
                    else amountETop += 2;
                    amountA--;
                    amountB--;
                }

                // Generation bottom
                int amountC = 1;
                int amountD = 1;
                int amountEBottom = amountEMiddle - 2;
                if (n == 2) amountEBottom = 0;

                while (amountEBottom >= 0)
                {
                    bottom = bottom.Append(new string('C', amountC) + "+");
                    if (amountEBottom > 0) bottom = bottom.Append(new string('E', amountEBottom) + "+");
                    bottom = bottom.Append(new string('D', amountD) + "\n");

                    if (amountEBottom - 2 == -1) amountEBottom = 0;
                    else amountEBottom -= 2;
                    amountC++;
                    amountD++;
                }
            }

            Console.Write(top);
            Console.WriteLine(middle);
            Console.Write(bottom);
            Console.WriteLine();
        }
    }
}