﻿using System;

namespace Easy2
{
    class Program
    {
        private static int input = 1;
        static void Main(string[] args)
        {
            while (input != 0)
            {
                Console.WriteLine("Enter a year: (exit with 0)");
                input = Int32.Parse(Console.ReadLine());
                if (input % 400 == 0 || (input % 400 != 0  && input % 100 != 0 && input % 4 == 0)) Console.WriteLine($"{input} -> true \n");
                else Console.WriteLine($"{input} -> false \n");
            }
        }
    }
}