﻿/*
 * (Python specific question)
 * else: will be called only when the the exception didn't occur.
 * finally: will always be called whether the exception occur or not.
 */