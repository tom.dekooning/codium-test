﻿using System;

namespace Easy1
{
    class Program
    {
        private const int MIN = 1;
        private const int MAX = 100;
        
        static void Main(string[] args)
        {
            for (int i = MIN; i <= MAX; i++)
            {
                if (i % 3 == 0 && i % 5 == 0) Console.Write("FizzBuzz ");
                else if (i % 3 == 0) Console.Write("Fizz ");
                else if (i % 5 == 0) Console.Write("Buzz ");
                else Console.Write($"{i} ");
            }
        }
    }
}