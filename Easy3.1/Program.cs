﻿using System;

namespace Easy3._1
{
    class Program
    {
        private static int[] numbers = {3, 4, 6,10};

        static void Main(string[] args)
        {
            foreach (var t in numbers)
            {
                ProduceOutput(t);
            }
        }

        private static void ProduceOutput(int n)
        {
            Console.WriteLine($"n={n}");
            for (int i = 0; i < n; i++)
            {
                Console.Write("*");
                if (i != 0)
                    for (int j = 0; j < i; j++)
                    {
                        Console.Write("*");
                    }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}