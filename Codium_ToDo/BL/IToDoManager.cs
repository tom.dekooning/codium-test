﻿using System.Collections.Generic;

namespace Codium_ToDo.BL
{
    public interface IToDoManager
    {
        List<string> RemoveToDo(int toDoNumber, List<string> toDos);
        void SaveToDosOnRedis(List<string> toDos);
        List<string> GetToDos();
        void SaveToDos(List<string> toDos);
    }
}