﻿using System.Collections.Generic;
using Codium_ToDo.DAL;

namespace Codium_ToDo.BL
{
    public class TodoManager : IToDoManager
    {
        private static IToDoRepo toDoRepo = new ToDoRepo();
        
        public List<string> RemoveToDo(int toDoNumber, List<string> toDos)
        {
            return toDoRepo.DeleteToDo(toDoNumber, toDos);
        }

        public void SaveToDosOnRedis(List<string> toDos)
        {
            toDoRepo.PersistToRedis(toDos);
        }

        public List<string> GetToDos()
        {
            return toDoRepo.ReadStorage();
        }

        public void SaveToDos(List<string> toDos)
        {
            toDoRepo.WriteToStorage(toDos);
        }
    }
}