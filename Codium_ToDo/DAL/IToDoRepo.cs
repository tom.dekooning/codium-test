﻿using System.Collections.Generic;

namespace Codium_ToDo.DAL
{
    public interface IToDoRepo
    {
        List<string> DeleteToDo(int toDoNumber, List<string> toDos);
        void PersistToRedis(List<string> toDos);
        List<string> ReadStorage();
        void WriteToStorage(List<string> toDos);
    }
}