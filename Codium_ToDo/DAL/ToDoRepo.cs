﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Codium_ToDo.DAL
{
    public class ToDoRepo : IToDoRepo
    {
        private const string FILENAME = "todos.txt";
        
        public List<string> DeleteToDo(int toDoNumber, List<string> toDos)
        {
            if (toDoNumber == 0) return null;
            if (toDoNumber-1 > toDos.Count)
                Console.WriteLine("Invalid number: please enter a good number");
            else
            {
                toDos.Remove(toDos[toDoNumber - 1]);
                WriteToStorage(toDos);
            }
            return toDos;
        }

        public void PersistToRedis(List<string> toDos)
        {
            ConnectionMultiplexer muxer = ConnectionMultiplexer.Connect("34.77.115.255:6379,password=");
            IDatabase conn = muxer.GetDatabase();
            conn.StringSet("todos", string.Join(" ",toDos.ToArray()));
        }

        public List<string> ReadStorage()
        {
            List<string> fetchedToDos = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText(FILENAME));
            return fetchedToDos;
        }

        public void WriteToStorage(List<string> toDos)
        {
            File.WriteAllText(FILENAME, JsonConvert.SerializeObject(toDos));
        }
    }
}