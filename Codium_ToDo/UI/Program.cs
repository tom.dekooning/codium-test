﻿/*
 * Codium Test To-Do Application
 * tom.dekooning@student.kdg.be
 * Tom De Kooning
 */

using System;
using System.Collections.Generic;
using Codium_ToDo.BL;

namespace Codium_ToDo.UI
{
    class Program
    {
        private static string input;
        private static List<string> toDos = new List<string>();
        private static IToDoManager toDoManager = new TodoManager();
        private const int AMOUNT_OF_LINES = 50;

        static void Main(string[] args)
        {
            // TEST IF DATA IS ACTUALLY PERSISTED TO THE GOOGLE CLOUD REDIS SERVER.
            // ConnectionMultiplexer muxer = ConnectionMultiplexer.Connect("34.77.115.255:6379,password=");
            // IDatabase conn = muxer.GetDatabase();
            // var value = conn.StringGet("todos");
            // Console.WriteLine($"Todos on Redis: {value}");

            while (input != "0")
            {
                Console.WriteLine("To-Do List Codium Command-Line v1.0");
                var currentToDos = toDoManager.GetToDos();
                if (currentToDos != null)
                {
                    toDos = currentToDos;
                    PrintToDos();
                }
                Console.WriteLine(
                    "MENU\n" +
                    "1. Add a todo\n" +
                    "2. Remove a todo\n" +
                    "3. Persist to Redis\n" +
                    "0. Exit");
                Console.WriteLine(new String('-', AMOUNT_OF_LINES));
                
                input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        Add();
                        break;
                    case "2":
                        Remove();
                        break;
                    case "3":
                        toDoManager.SaveToDosOnRedis(toDos);
                        break;
                    case "0": break;
                }
            }
        }

        private static void PrintToDos()
        {
            Console.WriteLine(new String('-', AMOUNT_OF_LINES));
            Console.WriteLine(toDos.Count >= 0 ? "Your current todos: " : "You don't have any todos.");
            for (var i = 0; i < toDos.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {toDos[i]}");
            }

            Console.WriteLine(new String('-', AMOUNT_OF_LINES));
        }

        private static void Add()
        {
            while (input != "000")
            {
                Console.WriteLine("Enter a todo: (exit with 000)");
                input = Console.ReadLine();
                if (input != "000")
                {
                    toDos.Add(input);
                    toDoManager.SaveToDos(toDos);
                }
            }
        }

        private static void Remove()
        {
            while (input != "000" && toDos.Count != 0)
            {
                Console.WriteLine("Enter the number of the todo that you have completed: (exit with 000)");
                input = Console.ReadLine();
                int number = Int32.Parse(input);
                toDoManager.RemoveToDo(number, toDos);
                PrintToDos();
            }
        }
    }
}