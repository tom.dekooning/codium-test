﻿using System;
using System.Linq;
using System.Text;

namespace Easy3._3
{
    class Program
    {
        private static int[] numbers = {1, 2, 3, 4, 5,8};

        static void Main(string[] args)
        {
            foreach (var t in numbers)
            {
                ProduceOutput(t);
            }
        }

        private static void ProduceOutput(int n)
        {
            Console.WriteLine($"n={n}");
            for (int i = 0; i < n; i++)
            {
                if (i == 0)
                {
                    for (int j = 0; j < n-1; j++)
                    {
                        Console.Write(" ");
                    }

                    Console.Write("*");
                }
                else
                {
                    for (int j = 0; j < n - i-1; j++)
                    {
                        Console.Write(" ");
                    }
                    Console.Write("*");
                    StringBuilder spaces = new StringBuilder("");
                    for (int j = 0; j < i*2; j++)
                    {
                        spaces.Append(" ");
                    }
                    spaces.Remove(spaces.Length - 1, 1);
                    Console.Write(spaces);
                    Console.Write("*");
                }

                Console.WriteLine();
            }
        }
    }
}