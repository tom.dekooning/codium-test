﻿using System;
using System.Text;

namespace Easy3._5
{
    class Program
    {
        private static int[] numbers = {1, 2, 3, 4, 5, 9,15};

        static void Main(string[] args)
        {
            foreach (var t in numbers)
            {
                ProduceOutput(t);
            }
        }

        private static void ProduceOutput(int n)
        {
            Console.WriteLine($"n={n}");
            
            // Generation of middle
            StringBuilder middle = new StringBuilder();
            int amountOfAstrixMiddle = 0;
            if (n == 1)
            {
                Console.Write("*");
            }
            else if (n % 2 == 0)
            {
                amountOfAstrixMiddle = n - 1;
                for (int j = 0; j < 2; j++)
                {
                    for (int i = 0; i < n - 1; i++)
                    {
                        middle = middle.Append("*");
                    }
                    if (j == 0) middle.Append("\n");
                }
            }
            else
            {
                amountOfAstrixMiddle = n;
                for (int i = 0; i < n; i++)
                {
                    middle = middle.Append("*");
                }
            }

            StringBuilder top = new StringBuilder();
            StringBuilder bottom = new StringBuilder();
            if (n > 2)
            {
                // Generation of top
                int amountOfSpacesTop = (amountOfAstrixMiddle - 1) / 2;
                int amountOfAstrixTop = 1;
                while (amountOfSpacesTop > 0)
                {
                    for (int i = 0; i < amountOfSpacesTop; i++)
                    {
                        top = top.Append(" ");
                    }

                    for (int i = 0; i < amountOfAstrixTop; i++)
                    {
                        top = top.Append("*");
                    }

                    top = top.Append("\n");
                    amountOfAstrixTop += 2;
                    amountOfSpacesTop--;
                }

                // Generation of bottom
                int amountOfSpaceBottom = 1;
                int amountOfAstrixBottom = amountOfAstrixMiddle - 2;
                while (amountOfAstrixBottom > 0)
                {
                    for (int i = 0; i < amountOfSpaceBottom; i++)
                    {
                        bottom = bottom.Append(" ");
                    }

                    for (int i = 0; i < amountOfAstrixBottom; i++)
                    {
                        bottom = bottom.Append("*");
                    }

                    bottom = bottom.Append("\n");
                    amountOfAstrixBottom -= 2;
                    amountOfSpaceBottom++;
                }
            }

            Console.Write(top);
            Console.WriteLine(middle);
            Console.Write(bottom);
            Console.WriteLine();
        }
    }
}