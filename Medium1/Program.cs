﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace Medium1
{
    class Program
    {
        private static string input = "";
        private static int number = 0;

        static void Main(string[] args)
        {
            while (input != "0")
            {
                Console.WriteLine(
                    "Enter x of the range [0-x], where you want to retrieve the prime numbers from: (exit with 0)");
                input = Console.ReadLine();
                number = Int32.Parse(input);
                var primeNumbers = GetPrimeNumbers(number);
                var primeString = String.Join(" ", primeNumbers);
                Console.WriteLine($"{number} -> {primeString}");
            }
        }

        private static List<int> GetPrimeNumbers(int n)
        {
            List<int> primes = new List<int>();
            for (int i = 1; i <= n; i++)
            {
                int factors = 0;
                for (int j = 1; j <= i; j++)
                {
                    if (i % j == 0) factors++;
                }
                if (factors == 2) primes.Add(i);
            }
            return primes;
        }
    }
}