﻿using System;
using System.Text;

namespace Easy3._4
{
    class Program
    {
        private static int[] numbers = {1, 2, 3, 4, 5, 6, 7,15};

        static void Main(string[] args)
        {
            foreach (var t in numbers)
            {
                ProduceOutput(t);
            }
        }

        private static void ProduceOutput(int n)
        {
            Console.WriteLine($"n={n}");

            StringBuilder top = new StringBuilder();
            StringBuilder middle = new StringBuilder();
            StringBuilder bottom = new StringBuilder();

            // Generation of middle
            int amountSpacesBottom;
            int amountSpacesTop = n - 2;
            if (n == 3) amountSpacesTop = 1;
            int rows;
            int amountIndentTop = 0;
            int amountIndentMiddle;
            if (n % 2 == 0)
            {
                amountIndentMiddle = (n - 2) / 2;
                middle = middle.Append(new string(' ', amountIndentMiddle) + "**\n" +
                                       new string(' ', amountIndentMiddle) + "**");
                amountSpacesBottom = 2;
                rows = 2;
            }
            else
            {
                amountIndentMiddle = (n - 1) / 2;
                middle = middle.Append(new string(' ', amountIndentMiddle) + "*");
                amountSpacesBottom = 1;
                rows = 1;
            }

            int amountIndentBottom = amountIndentMiddle - 1;
            
            while (rows < n)
            {
                top = top.Append(new string(' ',amountIndentTop) + "*" + new string(' ', amountSpacesTop) + "*\n");
                bottom = bottom.Append(new string(' ',amountIndentBottom) + "*" + new string(' ', amountSpacesBottom) + "*\n");
                rows += 2;
                amountSpacesBottom += 2;
                amountSpacesTop -= 2;
                amountIndentTop++;
                amountIndentBottom--;
            }

            Console.Write(top);
            Console.WriteLine(middle);
            Console.Write(bottom);
            Console.WriteLine();
        }
    }
}